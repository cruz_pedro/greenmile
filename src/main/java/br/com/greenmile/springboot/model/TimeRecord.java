package br.com.greenmile.springboot.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_time_record")
public class TimeRecord extends BaseEntity<Integer> implements Serializable{

	@Temporal(TemporalType.TIME)
	@DateTimeFormat(style = "hh:mm:ss")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="hh:mm:ss")
	@Column(name="time", nullable = false)
	private Date time;
	
	@ManyToOne(optional = false)
	private User user;

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
