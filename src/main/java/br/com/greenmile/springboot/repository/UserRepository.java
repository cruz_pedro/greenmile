package br.com.greenmile.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.greenmile.springboot.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);
}
