package br.com.greenmile.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.greenmile.springboot.model.TimeRecord;
import br.com.greenmile.springboot.model.User;

public interface TimeRecordRepository extends JpaRepository<TimeRecord, Integer>{

	List<TimeRecord> findByUser(User user);

}
