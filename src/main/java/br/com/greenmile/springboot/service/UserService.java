package br.com.greenmile.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.greenmile.springboot.model.User;
import br.com.greenmile.springboot.repository.UserRepository;

@Service
public class UserService {

	@Autowired
    private UserRepository repository;
	
    public User findByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username);
        return user;
    }
    
    public User save(User user) {
    	return repository.save(user);
    }
    
    public List<User> findAll(){
    	return repository.findAll();
    }
    
    public User getOne(Integer id){
    	return repository.findOne(id);
    }
    
    public void delete(User user){
    	repository.delete(user);
    }
}
