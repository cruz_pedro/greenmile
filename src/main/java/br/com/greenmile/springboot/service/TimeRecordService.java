package br.com.greenmile.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.greenmile.springboot.model.TimeRecord;
import br.com.greenmile.springboot.model.User;
import br.com.greenmile.springboot.repository.TimeRecordRepository;

@Service
public class TimeRecordService {

	@Autowired
    private TimeRecordRepository repository;
	
    public List<TimeRecord> findByUser(User user){
        return repository.findByUser(user);
    }
    
    public TimeRecord save(TimeRecord timeRecord) {
    	return repository.save(timeRecord);
    }
    
    public List<TimeRecord> findAll(){
    	return repository.findAll();
    }

    public TimeRecord getOne(Integer id){
    	return repository.getOne(id);
    }
    
    public void delete(TimeRecord timeRecord){
    	repository.delete(timeRecord);
    }
}
