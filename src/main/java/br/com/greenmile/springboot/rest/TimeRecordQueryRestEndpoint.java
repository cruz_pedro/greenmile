package br.com.greenmile.springboot.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.greenmile.springboot.model.TimeRecord;
import br.com.greenmile.springboot.model.User;
import br.com.greenmile.springboot.service.TimeRecordService;
import br.com.greenmile.springboot.service.UserService;

@RestController
@RequestMapping(value = "/public/timeRecorder")
public class TimeRecordQueryRestEndpoint {
	
	@Autowired
	private TimeRecordService timeRecordService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<List<TimeRecord>> findAll(){
		return new ResponseEntity<List<TimeRecord>>(timeRecordService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findByUser/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<TimeRecord>> findByUser(@PathVariable Integer id){
		User user = userService.getOne(id);

		if (user == null) {
			return new ResponseEntity<List<TimeRecord>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<TimeRecord>>(timeRecordService.findByUser(user), HttpStatus.OK);
	}

}
