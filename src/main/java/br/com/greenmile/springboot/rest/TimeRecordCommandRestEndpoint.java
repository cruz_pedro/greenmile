package br.com.greenmile.springboot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.greenmile.springboot.model.TimeRecord;
import br.com.greenmile.springboot.service.TimeRecordService;

@RestController
@RequestMapping(value = "/auth/timeRecorder")
public class TimeRecordCommandRestEndpoint {

	@Autowired
	private TimeRecordService timeRecordService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<TimeRecord> save(@RequestBody TimeRecord timeRecord) {
		return new ResponseEntity<TimeRecord>(timeRecordService.save(timeRecord), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<TimeRecord> deleteUser(@PathVariable Integer id) {
		TimeRecord timeRecord = timeRecordService.getOne(id);
		if (timeRecord == null) {
			return new ResponseEntity<TimeRecord>(HttpStatus.NO_CONTENT);
		} else {
			timeRecordService.delete(timeRecord);
			return new ResponseEntity<TimeRecord>(HttpStatus.OK);
		}
	}
}
