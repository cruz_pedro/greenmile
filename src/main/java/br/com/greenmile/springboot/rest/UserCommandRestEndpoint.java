package br.com.greenmile.springboot.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.Consumes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.greenmile.springboot.model.User;
import br.com.greenmile.springboot.service.UserService;
import br.com.greenmile.springboot.util.PasswordUtil;

@RestController
@RequestMapping(value = "/auth/user")
@Consumes("application/json;charset=UTF-8")
public class UserCommandRestEndpoint {

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<User> createUser(@RequestBody User user) {
		if (Objects.nonNull(userService.findByUsername(user.getUsername()))) {
			throw new RuntimeException("Username already exist");
		}

		List<String> roles = new ArrayList<>();
		roles.add("USER");
		user.setRoles(roles);
		user.setPassword(PasswordUtil.digestPassword(user.getPassword()));
		return new ResponseEntity<User>(userService.save(user), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public User updateUser(@RequestBody User user) {
		User savedUser = userService.findByUsername(user.getUsername());
		if (Objects.nonNull(savedUser)	&& !Objects.equals(savedUser.getId(), user.getId())){
			throw new RuntimeException("Username already exist");
		}
		return userService.save(user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteUser(@PathVariable Integer id) {
		User user = userService.getOne(id);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String loggedUsername = auth.getName();

		if (user == null) {
			return new ResponseEntity<User>(HttpStatus.NO_CONTENT);

		} else if (user.getUsername().equalsIgnoreCase(loggedUsername)) {
			throw new RuntimeException("You cannot delete your account");

		} else {
			userService.delete(user);
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
	}

}
